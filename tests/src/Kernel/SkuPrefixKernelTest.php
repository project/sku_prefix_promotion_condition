<?php

namespace Drupal\Tests\sku_prefix_promotion_condition\Kernel;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductType;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_promotion\Entity\Promotion;
use Drupal\Tests\commerce_order\Kernel\OrderKernelTestBase;

/**
 * Tests promotion conditions.
 *
 * @group sku_prefix_promotion_condition
 */
class SkuPrefixKernelTest extends OrderKernelTestBase {

  /**
   * The test order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * Order item 1.
   *
   * @var \Drupal\commerce_order\Entity\OrderItemInterface
   */
  protected $firstOrderItem;

  /**
   * Order item 2.
   *
   * @var \Drupal\commerce_order\Entity\OrderItemInterface
   */
  protected $secondOrderItem;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'commerce_promotion',
    'commerce_promotion_test',
    'sku_prefix_promotion_condition',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();

    $this->installEntitySchema('commerce_promotion');
    $this->installConfig(['commerce_promotion']);
    $this->installSchema('commerce_promotion', ['commerce_promotion_usage']);

    $user = $this->createUser();

    $this->order = Order::create([
      'type' => 'default',
      'state' => 'completed',
      'mail' => 'test@example.com',
      'ip_address' => '127.0.0.1',
      'order_number' => '6',
      'store_id' => $this->store,
      'uid' => $user->id(),
      'order_items' => [],
    ]);
  }

  /**
   * Helper.
   */
  protected function addProductsAndOrderItems() {
    $product_type = ProductType::create([
      'id' => 'test',
      'label' => 'Test',
      'variationType' => 'default',
    ]);
    $product_type->save();
    $first_variation = ProductVariation::create([
      'type' => 'default',
      'sku' => '1234567',
      'price' => [
        'number' => '20',
        'currency_code' => 'USD',
      ],
    ]);
    $first_variation->save();
    $second_variation = ProductVariation::create([
      'type' => 'default',
      'sku' => '66654431',
      'price' => [
        'number' => '30',
        'currency_code' => 'USD',
      ],
    ]);
    $second_variation->save();

    $first_product = Product::create([
      'type' => 'default',
      'title' => $this->randomMachineName(),
      'stores' => [$this->store],
      'variations' => [$first_variation],
    ]);
    $first_product->save();
    $second_product = Product::create([
      'type' => 'test',
      'title' => $this->randomMachineName(),
      'stores' => [$this->store],
      'variations' => [$second_variation],
    ]);
    $second_product->save();

    /** @var \Drupal\commerce_order\OrderItemStorageInterface $order_item_storage */
    $order_item_storage = $this->container->get('entity_type.manager')->getStorage('commerce_order_item');
    $first_order_item = $order_item_storage->createFromPurchasableEntity($first_variation);
    $first_order_item->save();
    $second_order_item = $order_item_storage->createFromPurchasableEntity($second_variation);
    $second_order_item->save();
    $this->order->setItems([$first_order_item, $second_order_item]);
    $this->order->set('state', 'draft');
    $this->order->save();
    $this->order = $this->reloadEntity($this->order);
    $this->firstOrderItem = $this->reloadEntity($first_order_item);
    $this->secondOrderItem = $this->reloadEntity($second_order_item);
  }

  /**
   * Test the order item plugin.
   *
   * @dataProvider getPrefixes
   */
  public function testPrefixConditionOrderItem($sku_config) {
    // Starts now, enabled. No end time.
    $promotion = Promotion::create([
      'name' => 'Promotion 1',
      'order_types' => [$this->order->bundle()],
      'stores' => [$this->store->id()],
      'status' => TRUE,
      'offer' => [
        'target_plugin_id' => 'order_item_percentage_off',
        'target_plugin_configuration' => [
          'conditions' => [
            [
              'plugin' => 'sku_prefix',
              'configuration' => [
                'sku_prefix' => $sku_config,
              ],
            ],
          ],
          'percentage' => '0.10',
        ],
      ],
    ]);
    $promotion->save();

    $this->addProductsAndOrderItems();

    $this->assertCount(1, $this->firstOrderItem->getAdjustments());
    $this->assertCount(0, $this->secondOrderItem->getAdjustments());
  }

  /**
   * Test the order item plugin.
   *
   * @dataProvider getPrefixes
   */
  public function testPrefixConditionOrder($sku_config) {
    // Starts now, enabled. No end time.
    $promotion = Promotion::create([
      'name' => 'Promotion 1',
      'order_types' => [$this->order->bundle()],
      'stores' => [$this->store->id()],
      'status' => TRUE,
      'offer' => [
        'target_plugin_id' => 'order_item_percentage_off',
        'target_plugin_configuration' => [
          'percentage' => '0.10',
        ],
      ],
      'conditions' => [
        [
          'target_plugin_id' => 'sku_prefixes_on_order',
          'target_plugin_configuration' => [
            'sku_prefixes' => $sku_config,
          ],
        ],
      ],
      'condition_operator' => 'AND',
    ]);
    $promotion->save();

    $this->addProductsAndOrderItems();

    // This will make the first one, that actually has the SKU in it, have an
    // adjustment.
    $this->assertCount(1, $this->firstOrderItem->getAdjustments());
    // This one does not match the SKU, but the rule is to get an order item
    // adjustment when a SKU in your _order_ matches the prefix. Which the first
    // one does, as said.
    $this->assertCount(1, $this->secondOrderItem->getAdjustments());
  }

  /**
   * Data provider for the tests.
   */
  public function getPrefixes() {
    return [
      [
        "4123\n0987\n123",
      ],
      [
        "4123,0987,123",
      ],
      [
        "4123\n0987,123",
      ],
      [
        "\n\n4123\n,,\n\n,,,,\n\n,\n0987,123",
      ],
      [
        "\n\n4123\n0987\n123",
      ],
      [
        "4123\n0987\n\n123\n",
      ],
    ];
  }

}
