<?php

namespace Drupal\Tests\sku_prefix_promotion_condition\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test that we can enable the module.
 *
 * @group sku_prefix_promotion_condition
 */
class ModuleEnableTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'sku_prefix_promotion_condition',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A simple test that enables the module.
   */
  public function testEnable() {
    // Assert something so the test runs.
    self::assertTrue(TRUE);
  }

}
