<?php

namespace Drupal\Tests\sku_prefix_promotion_condition\Unit;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\sku_prefix_promotion_condition\Plugin\Commerce\Condition\SkuPrefix;
use Drupal\sku_prefix_promotion_condition\Plugin\Commerce\Condition\SkuPrefixInOrder;
use Drupal\Tests\UnitTestCase;

/**
 * Test that the evaluation works as expected.
 *
 * @group sku_prefix_promotion_condition
 */
class SkuPrefixUnitTest extends UnitTestCase {

  /**
   * Test different scenarios.
   *
   * @dataProvider provideConfigAndSku
   */
  public function testEvaluateOrderItemCondition($config, $sku, $expected_result) {
    $prefix_condition = new SkuPrefix([
      'sku_prefix' => $config,
    ], 'bogus', [
      'entity_type' => 'commerce_order_item',
    ]);
    $variation = $this->createMock(ProductVariationInterface::class);
    $variation->method('getSku')
      ->willReturn($sku);
    $order_item = $this->createMock(OrderItemInterface::class);
    $order_item->method('getEntityTypeId')
      ->willReturn('commerce_order_item');
    $order_item->method('getPurchasedEntity')
      ->willReturn($variation);
    $variation->method('getEntityTypeId')
      ->willReturn('commerce_product_variation');
    $this->assertEquals($expected_result, $prefix_condition->evaluate($order_item));
  }

  /**
   * Test the order condition.
   *
   * @dataProvider provideConfigAndSkuForOrder
   */
  public function testEvaluateOrderCondition($config, $sku, $expected_result, $exclude = FALSE) {
    $prefix_condition = new SkuPrefixInOrder([
      'sku_prefixes' => $config,
      'exclude' => $exclude,
    ], 'bogus', [
      'entity_type' => 'commerce_order',
    ]);
    $variation = $this->createMock(ProductVariationInterface::class);
    $variation->method('getSku')
      ->willReturn($sku);
    $order_item = $this->createMock(OrderItemInterface::class);
    $order_item->method('getEntityTypeId')
      ->willReturn('commerce_order_item');
    $order_item->method('getPurchasedEntity')
      ->willReturn($variation);
    $variation->method('getEntityTypeId')
      ->willReturn('commerce_product_variation');
    $order = $this->createMock(OrderInterface::class);
    $order->method('getItems')
      ->willReturn([$order_item]);
    $order->method('getEntityTypeId')
      ->willReturn('commerce_order');
    $this->assertEquals($expected_result, $prefix_condition->evaluate($order));
  }

  /**
   * A data provider.
   */
  public function provideConfigAndSkuForOrder() {
    return array_merge($this->provideConfigAndSku(), [
      [
        "21,12",
        1234,
        FALSE,
        TRUE,
      ],
      [
        "1\n2\n3\n4",
        1234,
        FALSE,
        TRUE,
      ],
      [
        21,
        1234,
        TRUE,
        TRUE,
      ],
    ]);
  }

  /**
   * A dataprovider.
   */
  public function provideConfigAndSku() {
    return [
      [
        12,
        1234,
        TRUE,
      ],
      [
        12,
        3456,
        FALSE,
      ],
      [
        '12',
        '1234',
        TRUE,
      ],
      [
        12,
        '1234',
        TRUE,
      ],
      [
        '666,123,999',
        999111,
        TRUE,
      ],
      [
        "666,123,1999\n988978,96,9",
        999111,
        TRUE,
      ],
      [
        '666,123,999',
        1999111,
        FALSE,
      ],
    ];
  }

}
